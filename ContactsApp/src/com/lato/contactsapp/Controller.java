package com.lato.contactsapp;

import contactsdatamodel.Contact;
import contactsdatamodel.ContactData;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;

public class Controller {

    @FXML
    private BorderPane mainWindow;

    @FXML
    private TableView<Contact> contactsTableView;

    private ContactData data;

    public void initialize() {
        data = new ContactData();
        data.loadContacts();
        contactsTableView.setItems(data.getContact());
    }

    @FXML
    public void showNewContactDialog() {
        Dialog<ButtonType> dialog = new Dialog<ButtonType>();
        dialog.initOwner(mainWindow.getScene().getWindow());
        dialog.setTitle("Add new contact");
        dialog.setHeaderText("Use this dialog to add new contact");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("newcontact.fxml"));
        try {
            dialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            System.out.println("Something went wrong with loading dialog!");
            e.printStackTrace();
            return;
        }

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add((ButtonType.CANCEL));

        Optional<ButtonType> result = dialog.showAndWait();
        if(result.isPresent() && result.get() == ButtonType.OK) {
            ContactController controller = fxmlLoader.getController();
            Contact contact = controller.getContact();
            data.addContact(contact);
            data.saveContacts();
        } else {
            System.out.println("Operation canceled");
        }
    }

    @FXML
    public void showEditContactDialog() { // do zrobienia
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainWindow.getScene().getWindow());
        dialog.setTitle("Edit this contact: ");
        dialog.setHeaderText("Use this dialog to change contact details");
        FXMLLoader fxmlLoader = new FXMLLoader();

    }

}
