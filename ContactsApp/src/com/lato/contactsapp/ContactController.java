package com.lato.contactsapp;

import contactsdatamodel.Contact;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;


public class ContactController {

    @FXML
    private TextField fieldFirstName;

    @FXML
    private TextField fieldLastName;

    @FXML
    private TextField fieldPhoneNumber;

    @FXML
    private TextField fieldNotes;

    public Contact getContact() {
    String firstName = fieldFirstName.getText();
    String lastName = fieldLastName.getText();
    String phoneNumber = fieldPhoneNumber.getText();
    String notes = fieldNotes.getText();

    Contact contact = new Contact(firstName, lastName, phoneNumber, notes);
    return contact;
    }

}
