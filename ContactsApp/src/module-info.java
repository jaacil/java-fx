module ContactsApp {
    requires javafx.fxml;
    requires javafx.controls;
    requires java.xml;

    exports contactsdatamodel;

    opens com.lato.contactsapp;
}