module HelloWorldFX {
    requires javafx.fxml;
    requires javafx.controls;
    requires jlfgr;

    opens com.lato.todolist;
}